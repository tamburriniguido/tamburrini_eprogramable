/*==================[CONSIGNA D]=============================================*/
/*
Escribir una función que reciba como parámetro un dígito BCD y
un vector de estructuras del tipo  gpioConf_t.

typedef struct
{
//	uint8_t port;				!< GPIO port number
//uint8_t pin;				/*!< GPIO pin number */
//uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
//} gpioConf_t;
/*
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14


La función deberá establecer en qué valor colocar cada bit del dígito BCD
e indexando el vector anterior operar sobre el puerto y pin que corresponda.
*/

/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
}gpioConf_t;

void ControlPuertos(uint8_t bcd, gpioConf_t *sport_ptr)
{
uint8_t i=0;
for(i=0; i<4; i++)//va de 0 a 4 dado q sabes el largo del numero (bcd)
	{
	printf("El puerto %d", sport_ptr ->port);

	if(bcd%2 == 1)
		{printf("esta en alto\r\n");}
	else
		{printf("esta en bajo\r\n");}

	bcd= bcd/2;
	sport_ptr++;//corres el puntero un lugar
	}
}

int main(void)
{
    uint8_t bcd;
    gpioConf_t puertos[4];//creas un vector de tipo  gpioConf_t
    gpioConf_t *sport_ptr;//creas un puntero del tipo  gpioConf_t

    puertos[0].port=14;
    puertos[1].port=15;
    puertos[2].port=16;
    puertos[3].port=214;

    sport_ptr = puertos;//al puntero le asigno el vector

    printf("Introduce un numero de digitos: ");
    scanf("%"SCNu8, &bcd);
    printf("Ingreso: %"PRIu8"\n",bcd);

    ControlPuertos(bcd, sport_ptr);


	return 0;
}

/*==================[end of file]============================================*/

