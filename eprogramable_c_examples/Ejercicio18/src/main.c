/*==================[CONSIGNA 18]=============================================*
/*Al ejercicio anterior agregue un número más (16 en total), modifique su
 programa de manera que agregue el número extra a la suma e intente no
 utilizar el operando división “/”
N°16 =>233. Si utiliza las directivas de preprocesador
( #ifdef, #ifndef,#endif, etc) no necesita generar un nuevo programa.
*/

/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
uint8_t vector[]={233, 32, 100, 9, 101, 214, 129,111, 24, 1, 123, 211, 222, 234, 116, 124};

int main(void)
{
	uint16_t suma = 0;
	int8_t promedio = 0;
	int8_t n_muestras = 0;
	 for (i= 0; i<16; i++)
	    {
	    	suma +=  vector[i];
	    }

	 promedio=(uint8_t)(suma>>(n_muestras/4));
	 printf("Promedio: %d", promedio);

	return 0;
}

/*==================[end of file]============================================*/

