/*==================[CONSIGNA]=============================================*/
/*
 Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida
  y un puntero a un arreglo donde se almacene los n dígitos. La función deberá
  convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida
  en el arreglo pasado como puntero.

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
}
*/
/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
/*Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida
  y un puntero a un arreglo donde se almacene los n dígitos. La función deberá
  convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida
  en el arreglo pasado como puntero.*/

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number )
{

	uint8_t i = 0;
	for (i= 0; i<digits; i++)
	{

		*bcd_number = data % 10; //nos da el resto
		printf ("%d\n ",*bcd_number);
		data = data/10;
		bcd_number ++;//corresel puntero


	}


}

int main(void)
{
	uint32_t dato = 234;
	uint8_t n_digito = 3;
	uint8_t bcd_vector[10];

	uint8_t *puntero_bcd;
	puntero_bcd = bcd_vector;

	BinaryToBcd (dato, n_digito, puntero_bcd);

	return 0;
}

/*==================[end of file]============================================*/

