
#ifndef _MAIN_H
#define _MAIN_H
/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>


/*==================[macros and definitions]=================================*/
int main(void);



/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

