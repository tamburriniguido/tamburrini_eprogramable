/*==================[CONSIGNA 17]=============================================*/

/*Realice un programa que calcule el promedio de los 15 números listados abajo,
para ello, primero realice un diagrama de flujo similar al presentado
en el ejercicio 9. (Puede utilizar la aplicación Draw.io).
Para la implementación, utilice el menor tamaño de datos posible:
*/


/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
uint8_t vector[]={32, 100, 9, 101, 214, 129,111, 24, 1, 123, 211, 222, 234, 116, 124};
int main(void)
{
	uint16_t suma = 0;
	int8_t i, promedio = 0;

    for (i= 0; i<15; i++)
    {
    	suma +=  vector[i];
    }

    promedio =(int8_t) (suma/15);
    printf("Promedio: %d", promedio);

	return 0;
}

/*==================[end of file]============================================*/

