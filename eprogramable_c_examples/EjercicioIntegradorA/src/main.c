/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 /*Realice un función  que reciba un puntero a una estructura LED como la que se muestra a continuación:
struct leds
{
	uint8_t n_led;      indica el número de led a controlar
	uint8_t n_ciclos;   indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    indica el tiempo de cada ciclo
	uint8_t mode;       ON, OFF, TOGGLE
} my_leds;

Use como guía para la implementación el siguiente diagrama de flujo:
*/


/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t n_led;      //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON, OFF, TOGGLE
} my_leds;

void prende_led (uint8_t num_led)
{

};

void apaga_led (uint8_t num_led)
{
};
void retardo (uint8_t peri)
{
};
void toggle (uint8_t num_led, uint8_t num_ciclos, uint8_t periodo )
{
	uint8_t j = 0;

	while (j < num_ciclos)
	{
		prende_led(num_led);
		retardo(periodo);
		apaga_led(num_led);
		retardo(periodo);
		j++;
	}
};


void actua_led(my_leds *led_prueba_puntero)
{
	switch (led_prueba_puntero-> mode)
		{
		case (1):
			prende_led (led_prueba_puntero -> n_led);
			break;

		case (0):
			apaga_led (led_prueba_puntero -> n_led);
			break;

		case (2):
			toggle (led_prueba_puntero ->n_led,led_prueba_puntero-> n_ciclos, led_prueba_puntero -> periodo);
			break;

		}
}
/*==================[internal functions declaration]=========================*/



int main(void)
{
	my_leds led_prueba, *led_prueba_puntero;
	led_prueba_puntero = &led_prueba;

	led_prueba.n_led = 1;
	led_prueba.n_ciclos = 10;
	led_prueba.periodo = 4;
	led_prueba.mode = 0; //0 = off, 1 = on, 2 = toggle

	actua_led (led_prueba_puntero);

	return 0;
};

/*==================[end of file]============================================*/

